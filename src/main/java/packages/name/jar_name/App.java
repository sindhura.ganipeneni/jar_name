package packages.name.jar_name;

/**
 * Hello world!
 *
 */
public class App 
{
	String word="HelloWorld!";
	App(String word)
	{
		this.word=word;
	}
    public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
	public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }
}
